# Mkonstantinou/SystemSettings package

This is a laravel package that provides ease
on managing custom system settings stored locally and in database

## Requirements

- Laravel's illumination packages (to be a laravel project)

## Installation

- Load in composer
- Don't forget to publish the database migration of the package
## Usage

**Generate** a config file based on the settings in the database
```php
\Mkonstantinou\SystemSettings\Classes\Facades\SystemSettings::generate()
```

#### CRUD operations 
All Create-Update-Delete operations will also (re-)generate the file

**Add** a new record (and regenerate file)
```php
$data = [
    'abbr' => 'test',
    'name' => 'Test settings',
    'value' => '{key: value}'
];

\Mkonstantinou\SystemSettings\Classes\Facades\SystemSettings::add($data);
```

**Update** an existing record (and regenerate file)
```php
\Mkonstantinou\SystemSettings\Classes\Facades\SystemSettings::update($systemSettingsItem, $updatedData)
```

**Delete** an existing record (and regenerate file)
```php
\Mkonstantinou\SystemSettings\Classes\Facades\SystemSettings::delete($systemSettingsItem)
```

**Get** value
```php
\Mkonstantinou\SystemSettings\Classes\Facades\SystemSettings::get('test.key')
```

