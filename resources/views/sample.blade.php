<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Example</title>
    <!-- LOAD JSON-EDITOR LIBRARIES -->
    <link href="{{ asset('vendor/mkonstantinou/system-settings/css/jsoneditor.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('vendor/mkonstantinou/system-settings/js/jsoneditor.js') }}"></script>
    <script src="{{ asset('vendor/mkonstantinou/system-settings/js/jsoneditorinitializer.js') }}"></script>
</head>
<body>
<h1>USE THIS VIEW ONLY AS AN EXAMPLE</h1>

<div id="jsoneditor" style="width: 400px; height: 400px;"></div>

<script>
    // Main
    // Initialize data. Is it an edit page to load data?
    var editor;
    const jsonData = '{!! $item->value !!}';
    initEditor(jsonData);
</script>
</body>
</html>
