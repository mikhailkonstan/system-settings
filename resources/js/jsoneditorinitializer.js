// Initialize json Editor with data if any
function initEditor(jsonData){

    // create the editor
    const container = document.getElementById("jsoneditor");
    const options = {};
    editor = new JSONEditor(container, options);

    // set json to json editor or assign a default value
    if(jsonData !== null){
        editor.set(JSON.parse(jsonData));
    } else {
        const initialJson = {
            "place_option": [1, 2, 3],
        };

        editor.set(initialJson);
    }
}

// Wait for DOM to load
$( document ).ready(function() {

    // Handles button click for update & create
    $('#btn-store').click(function(e) {
        e.preventDefault();

        // get json
        const updatedJson = JSON.stringify(editor.get());
        $('#json-value').val(updatedJson);
        $('form').submit();
    });
});
