<?php

namespace Mkonstantinou\SystemSettings\Classes\Facades;

use Illuminate\Support\Facades\Facade;
use Mkonstantinou\SystemSettings\Models\SystemSettingsItem;

/**
 * @method static get(string $value, mixed $default = null): mixed
 * @method static getString(string $value, string $default = ''): string
 * @method static getInteger(string $value, ?int $default = null): int
 * @method static getBoolean(string $value, bool $default = false): bool
 * @method static getArray(string $value, array $default = []): array
 * @method static add(array $data): bool
 * @method static addOrUpdateValues(array $data): bool
 * @method static update(int $id, array $data): bool
 * @method static delete(SystemSettingsItem $item): bool
 * @method static generate(): bool
 **/
class SystemSettings extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'systemSettings';
    }
}
