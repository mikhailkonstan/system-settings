<?php

namespace Mkonstantinou\SystemSettings\Classes;

use Mkonstantinou\SystemSettings\Models\SystemSettingsItem;
use Mkonstantinou\SystemSettings\Services\SystemSettingsConfigFileService;
use Mkonstantinou\SystemSettings\Services\SystemSettingsItemService;

class SystemSettings
{
    public function __construct(
        private SystemSettingsItemService $itemService,
        private SystemSettingsConfigFileService $configFileService
    )
    {
    }

    /**
     * Adds a new SystemSettingsItem into the database and (re-)generates the config file
     *
     * @param array $data
     * @return bool
     */
    public function add(array $data): bool
    {
        // 1: Add Settings item to the database
        $result = $this->itemService->create($data);

        // 2: (Re-)generate config file and return actions' result
        return $result && $this->generate();
    }

    public function addOrUpdateValues(array $data): bool
    {
        $existingRecord = SystemSettingsItem::where('abbr', $data['abbr'])->first();

        // Record does not exist: Proceed with regular add operation
        if ($existingRecord === null) {
            return $this->add($data);
        }

        // Record exists. Iterate one by one its values and synchronize it with the values given as argument
        $existingValues = json_decode($existingRecord->value, true);
        $givenValues = json_decode($data['value'], true);

        foreach($givenValues as $key => $value) {
            if (array_key_exists($key, $existingValues) === false) {
                $existingValues[$key] = $value;
            }
        }


        return $this->update($existingRecord->id, [
            'abbr' => $existingRecord->abbr,
            'name' => $existingRecord->name,
            'description' => $existingRecord->description,
            'value' => json_encode($existingValues)
        ]);
    }

    /**
     * Returns the value of the given setting
     *
     * @param string $value
     * @param mixed $default
     * @return mixed
     */
    public function get(string $value, mixed $default = null): mixed
    {
        return $this->configFileService->get($value, $default);
    }

    /**
     * Returns the value of the given setting as string
     *
     * @param string $value
     * @param string $default
     * @return string
     */
    public function getString(string $value, string $default = ''): string
    {
        return (string)($this->get($value, $default));
    }

    /**
     * Returns the value of the given setting as array
     * If the given setting is not a valid array, an empty array is returned
     *
     * @param string $value
     * @param array $default
     * @return array
     */
    public function getArray(string $value, array $default = []): array
    {
        $result = $this->get($value, $default);
        if (is_array($result) === false) {
            return [];
        }

        return $result;
    }

    /**
     * Returns the value of the given setting as an integer
     * If the given setting is not numeric, null is returned. Otherwise, the value is being cast to integer
     *
     * @param string $value
     * @param ?int $default
     * @return ?int
     */
    public function getInteger(string $value, ?int $default = null): ?int
    {
        $result = $this->get($value, $default);
        if (is_numeric($result) === false) {
            return null;
        }

        return (int)$result;
    }

    /**
     * Returns the value of the given setting as a Boolean
     * If the given setting is not a boolean, false is returned. Otherwise, the value is being cast to boolean
     *
     * @param string $value
     * @param bool $default
     * @return bool
     */
    public function getBoolean(string $value, bool $default = false): bool
    {
        $result = $this->get($value, $default);
        if (is_bool($result) === false) {
            return false;
        }

        return $result;
    }

    public function update(int $id, array $data): bool
    {
        // Update item in database
        $item = SystemSettingsItem::find($id);
        $result = $this->itemService->update($item, $data);

        // Generate config file and return the actions' result
        return $result && $this->generate();
    }

    public function generate(): bool
    {
        return $this->configFileService->generate();
    }

    public function delete(SystemSettingsItem $item): bool
    {
        // Delete item in database
        $result = $this->itemService->delete($item);

        // Generate config file and return the action's result
        return $result && $this->generate();
    }
}
