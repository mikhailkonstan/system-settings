<?php

namespace Mkonstantinou\SystemSettings\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SystemSettingsCreate extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'abbr' => 'required|min:2|unique:system_settings',
            'name' => 'required|'
        ];
    }
}
