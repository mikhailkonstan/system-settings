<?php

namespace Mkonstantinou\SystemSettings\Services;

use Mkonstantinou\SystemSettings\Models\SystemSettingsItem;

/**
 * The class handles the major operations for an actual item (setting) such as Create-Update-Delete
 */
class SystemSettingsItemService
{
    /**
     * Creates a new Item and returns whether the operation was successful or not
     *
     * @param array $data
     * @return bool
     */
    public function create(array $data): bool
    {
        $item = new SystemSettingsItem($data);

        return $item->save();
    }

    /**
     * Updates the given item based on the given array of data
     *
     * @param SystemSettingsItem $item
     * @param array $data
     * @return bool
     */
    public function update(SystemSettingsItem $item, array $data): bool
    {
        return $item->update([
            'abbr' => $data['abbr'],
            'name' => $data['name'],
            'description' => array_key_exists('description', $data) ? $data['description'] : '',
            'value' => $data['value'],
        ]);
    }

    /**
     * Destroys the given item. Returns false only if the item was not deleted from the db
     * If the model was not found, or deleted successfully, the function returns true
     *
     * @param SystemSettingsItem $item
     * @return bool
     */
    public function delete(SystemSettingsItem $item): bool
    {
        $result = $item->delete();
        if ($result === null) {
            return true;
        }

        return $result;
    }
}
