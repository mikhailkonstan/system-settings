<?php

namespace Mkonstantinou\SystemSettings\Services;

use Illuminate\Database\Eloquent\Collection;
use Mkonstantinou\SystemSettings\Models\SystemSettingsItem;

/**
 * The class is responsible for the operations around a laravel configuration file
 * It handles the creation of the files and retrieval of the data values
 */
class SystemSettingsConfigFileService
{
    public function generate(): bool
    {
        $configFilename = 'systemsettings' . '.php';
        $items = SystemSettingsItem::all();
        $actionResult = $this->generateConfigFile($items, $configFilename);

        return $actionResult !== false;
    }

    /**
     * Returns the value from the config file of system-settings
     *
     * @param string $value
     * @param mixed $default
     * @return mixed
     */
    public function get(string $value, mixed $default = null): mixed
    {
        return config('systemsettings.' . $value, $default);
    }

    /**
     * Writes to custom-configuration file
     *
     * @param Collection<SystemSettingsItem> $settings
     * @param string $config_filename
     * @return int|bool
     */
    private function generateConfigFile(Collection $settings, string $config_filename): int|bool
    {
        $all_options_export = "<?php " . PHP_EOL . PHP_EOL . "return [" . PHP_EOL;

        // Create a printable version for each option configuration
        foreach($settings as $item){
            $option_export = "'" . $item->abbr . "' => " . PHP_EOL;
            $option_export .= $this->convertToPrintableArray($item->getValue());
            $all_options_export .= $option_export;
            $all_options_export .= "," . PHP_EOL;
        }
        $all_options_export .= "];" . PHP_EOL;
        $file = base_path() . "/config/" . $config_filename;

        // Create & write a new config file based on options given
        return file_put_contents($file, $all_options_export);
    }

    /**
     * Create a new configuration file in a php config file structure
     *
     * @param array $value
     * @return string|array
     */
    private function convertToPrintableArray(array $value): string|array
    {
        // Make a string with a config-file structure
        $return_value = var_export($value, true);

        // Replace all 'array ()' elements with []
        $return_value = str_replace('array (',  '[', $return_value);
        return str_replace(')',  ']', $return_value);
    }
}
