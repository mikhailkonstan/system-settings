<?php

namespace Mkonstantinou\SystemSettings;

use Illuminate\Support\ServiceProvider;
use Mkonstantinou\SystemSettings\Classes\SystemSettings as SystemSettingsClass;
use Mkonstantinou\SystemSettings\Services\SystemSettingsConfigFileService;
use Mkonstantinou\SystemSettings\Services\SystemSettingsItemService;

class SystemSettingsProvider extends ServiceProvider
{
    const PATH_RESOURCES = __DIR__.'/../resources/';

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //Binding our class to service provider
        $this->app->bind('systemSettings',function(){
            return new SystemSettingsClass(new SystemSettingsItemService(), new SystemSettingsConfigFileService());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Publish Migrations
        $this->publishes([
            __DIR__.'/../database/migrations/2022_11_11_223956_create_system_settings_table.php'
            => database_path('migrations/2022_11_11_223956_create_system_settings_table.php'),
        ]);

        // Publish Resources
        $this->publishes([
            self::PATH_RESOURCES . 'js' => public_path('vendor/mkonstantinou/system-settings/js'),
            self::PATH_RESOURCES . 'css' => public_path('vendor/mkonstantinou/system-settings/css'),
            self::PATH_RESOURCES . 'img' => public_path('img'),
        ], 'System-Settings/Assets');
    }
}
