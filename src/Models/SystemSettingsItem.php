<?php

namespace Mkonstantinou\SystemSettings\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemSettingsItem extends Model
{
    use HasFactory;

    protected $fillable = ['abbr', 'name', 'description', 'value'];
    protected $table = 'system_settings';

    /**
     * Returns the item's json value as a php associative array
     *
     * @return array
     */
    public function getValue(): array
    {
        return json_decode($this->value, true);
    }
}
