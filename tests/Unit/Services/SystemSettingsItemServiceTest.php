<?php

namespace Mkonstantinou\SystemSettings\Tests\Unit\Services;

use Mkonstantinou\SystemSettings\Models\SystemSettingsItem;
use Mkonstantinou\SystemSettings\Services\SystemSettingsItemService;
use Tests\TestCase;

/**
 * @coversDefaultClass \Mkonstantinou\SystemSettings\Services\SystemSettingsItemService
 */
class SystemSettingsItemServiceTest extends TestCase
{
    private SystemSettingsItemService $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = new SystemSettingsItemService();
    }

    /**
     * @covers ::delete
     */
    public function testDeleteSuccessfully(): void
    {
        $item = $this->createMock(SystemSettingsItem::class);
        $item->expects(static::once())->method('delete')->willReturn(true);

        static::assertTrue($this->service->delete($item));
    }

    /**
     * @covers ::delete
     */
    public function testDeleteNonExistedItem(): void
    {
        $item = $this->createMock(SystemSettingsItem::class);
        $item->expects(static::once())->method('delete')->willReturn(null);

        static::assertTrue($this->service->delete($item));
    }

    /**
     * @covers ::delete
     */
    public function testDeleteFailed(): void
    {
        $item = $this->createMock(SystemSettingsItem::class);
        $item->expects(static::once())->method('delete')->willReturn(false);

        static::assertFalse($this->service->delete($item));
    }

    public function testUpdate(): void
    {
        $data = [
            'abbr' => 'testAbbr',
            'name' => 'Full name',
            'description' => '',
            'value' => json_encode(['some' => 'value']),
        ];

        $item = $this->createMock(SystemSettingsItem::class);
        $item->expects(static::once())->method('update')->with($data)->willReturn(true);

        static::assertTrue($this->service->update($item, $data));
    }
}
