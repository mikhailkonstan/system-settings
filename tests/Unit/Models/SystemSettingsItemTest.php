<?php

namespace Mkonstantinou\SystemSettings\Tests\Unit\Models;

use Mkonstantinou\SystemSettings\Models\SystemSettingsItem;
use Tests\TestCase;

class SystemSettingsItemTest extends TestCase
{
    public function testGetValue(): void
    {
        $expectedArray = ['testKey' => 'testValue'];

        // Create a new system settings item
        $item = new SystemSettingsItem();
        $item->value = json_encode($expectedArray);

        static::assertSame($expectedArray, $item->getValue());
    }
}
