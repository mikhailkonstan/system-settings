<?php

namespace Mkonstantinou\SystemSettings\Tests\Unit\Classes;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Mkonstantinou\SystemSettings\Classes\SystemSettings;
use Mkonstantinou\SystemSettings\Models\SystemSettingsItem;
use Mkonstantinou\SystemSettings\Services\SystemSettingsConfigFileService;
use Mkonstantinou\SystemSettings\Services\SystemSettingsItemService;
use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\CreatesApplication;
use Tests\TestCase;

/**
 * @coversDefaultClass \Mkonstantinou\SystemSettings\Classes\SystemSettings
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class SystemSettingsTest extends TestCase
{
    use RefreshDatabase;

    private SystemSettings $systemSettings;
    private SystemSettingsItem|MockInterface $staticSystemSettingsItem;

    private SystemSettingsItemService|MockObject $itemService;
    private SystemSettingsConfigFileService|MockObject $configFileService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->itemService = $this->createMock(SystemSettingsItemService::class);
        $this->configFileService = $this->createMock(SystemSettingsConfigFileService::class);
        $this->systemSettings = new SystemSettings($this->itemService, $this->configFileService);
        $this->staticSystemSettingsItem = Mockery::mock('overload:Mkonstantinou\SystemSettings\Models\SystemSettingsItem');
    }

    /**
     * @covers ::generate
     * @return void
     */
    public function testGenerateOnSuccess(): void
    {
        // Should return true on success
        $this->configFileService->expects(static::once())->method('generate')->willReturn(true);
        static::assertTrue($this->systemSettings->generate());
    }

    /**
     * @covers ::generate
     * @return void
     */
    public function testGenerateOnFailure(): void
    {
        // Should return false on failure
        $this->configFileService->expects(static::once())->method('generate')->willReturn(false);
        static::assertFalse($this->systemSettings->generate());
    }

    /**
     * @covers ::delete
     * @covers ::generate
     * @return void
     */
    public function testDelete(): void
    {
        $item = new SystemSettingsItem(['id' => 123]);
        $this->itemService->expects(static::once())->method('delete')->with($item)->willReturn(true);
        $this->configFileService->expects(static::once())->method('generate')->willReturn(true);
        static::assertTrue($this->systemSettings->delete($item));
    }

    /**
     * @covers ::delete
     * @covers ::generate
     * @return void
     */
    public function testDeleteOnFailureShouldNotGenerateFile(): void
    {
        $item = new SystemSettingsItem(['id' => 123]);
        $this->itemService->expects(static::once())->method('delete')->with($item)->willReturn(false);
        $this->configFileService->expects(static::never())->method('generate');
        static::assertFalse($this->systemSettings->delete($item));
    }

    /**
     * @covers ::add
     * @covers ::generate
     * @return void
     */
    public function testAdd(): void
    {
        $data = [
            'abbr' => 'test',
            'name' => 'Test name',
            'value' => '{"key":"value"}'
        ];

        $this->itemService->expects(static::once())->method('create')->with($data)->willReturn(true);
        $this->configFileService->expects(static::once())->method('generate')->willReturn(true);
        static::assertTrue($this->systemSettings->add($data));
    }

    /**
     * @covers ::add
     * @covers ::generate
     * @return void
     */
    public function testAddOnFailureShouldNotGenerateFile(): void
    {
        $data = [
            'abbr' => 'test',
            'value' => '{"key":"value"}'
        ];

        $this->itemService->expects(static::once())->method('create')->with($data)->willReturn(false);
        $this->configFileService->expects(static::never())->method('generate');
        static::assertFalse($this->systemSettings->add($data));
    }

    /**
     * @covers ::update
     * @covers ::generate
     * @return void
     */
    public function testUpdate(): void
    {
        $itemData = [
            'id' => 12,
            'abbr' => 'test',
            'name' => 'Test name',
            'value' => '{"key":"value"}'
        ];

        $newValues = [
            'id' => 12,
            'abbr' => 'test_new',
            'name' => 'Test new name',
            'value' => '{"key":"new value"}'
        ];
        $item = new SystemSettingsItem($itemData);

        $this->staticSystemSettingsItem
            ->shouldReceive('find')
            ->once()
            ->with(12)
            ->andReturn($item);

        $this->itemService->expects(static::once())->method('update')->with($item, $newValues)->willReturn(true);
        $this->configFileService->expects(static::once())->method('generate')->willReturn(true);
        static::assertTrue($this->systemSettings->update(12, $newValues));
    }

    /**
     * @covers ::update
     * @covers ::generate
     * @return void
     */
    public function testUpdateOnFailureShouldNotGenerateFile(): void
    {
        $itemData = [
            'id' => 12,
            'abbr' => 'test',
            'name' => 'Test name',
            'value' => '{"key":"value"}'
        ];

        $newValues = [
            'value' => '{"key":"new value"}'
        ];

        $item = new SystemSettingsItem($itemData);

        $this->staticSystemSettingsItem
            ->shouldReceive('find')
            ->once()
            ->with( 12)
            ->andReturn($item);

        $this->itemService->expects(static::once())->method('update')->with($item, $newValues)->willReturn(false);
        $this->configFileService->expects(static::never())->method('generate');
        static::assertFalse($this->systemSettings->update(12, $newValues));
    }

    /**
     * @covers ::get
     * @return void
     */
    public function testGet(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn('someValue');

        static::assertSame('someValue', $this->systemSettings->get('test.key'));
    }

    /**
     * @covers ::getString
     * @return void
     */
    public function testGetString(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn('someValue');

        static::assertSame('someValue', $this->systemSettings->getString('test.key'));
    }

    /**
     * If Config file returns an integer value, the value should be cast to string
     * @covers ::getString
     * @covers ::get
     * @return void
     */
    public function testGetStringOnInteger(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn(121);

        static::assertSame('121', $this->systemSettings->getString('test.key'));
    }

    /**
     * If Config file returns a null value, the value should be cast to an empty string
     * @covers ::getString
     * @covers ::get
     * @return void
     */
    public function testGetStringOnNull(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn(null);

        static::assertSame('', $this->systemSettings->getString('test.key'));
    }

    /**
     * @covers ::getArray
     * @covers ::get
     * @return void
     */
    public function testGetArray(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn([1, 2, 3]);

        static::assertSame([1, 2, 3], $this->systemSettings->getArray('test.key'));
    }

    /**
     * If the value being returned by the config file is not a valid array, then an empty array should be returned
     *
     * @covers ::getArray
     * @covers ::get
     * @return void
     */
    public function testGetArrayOnInteger(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn(123);

        static::assertSame([], $this->systemSettings->getArray('test.key'));
    }

    /**
     * @covers ::getInteger
     * @covers ::get
     * @return void
     */
    public function testGetInteger(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn(123);

        static::assertSame(123, $this->systemSettings->getInteger('test.key'));
    }

    /**
     * @covers ::getBoolean
     * @covers ::get
     * @return void
     */
    public function testGetBoolean(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn(true);

        static::assertTrue($this->systemSettings->getBoolean('test.key'));
    }

    /**
     * @covers ::getBoolean
     * @covers ::get
     * @return void
     */
    public function testGetBooleanOnInvalidBool(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn('not a bool');

        static::assertFalse($this->systemSettings->getBoolean('test.key'));
    }

    /**
     * @covers ::getBoolean
     * @covers ::get
     * @return void
     */
    public function testGetBooleanOnInvalidBoolReturnsDefault(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn(true);
        static::assertTrue($this->systemSettings->getBoolean('test.key', true));
    }

    /**
     * If the value being returned by the config file is not a valid number, then null should be returned
     *
     * @covers ::getInteger
     * @covers ::get
     * @return void
     */
    public function testGetIntegerOnNonNumericValue(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn("something invalid");

        static::assertSame(null, $this->systemSettings->getInteger('test.key'));
    }

    /**
     * If the value being returned by the config file is a valid number but not an integer,
     * the method should cast the number to a valid integer
     *
     * @covers ::getInteger
     * @covers ::get
     * @return void
     */
    public function testGetIntegerOnFloat(): void
    {
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn(12.52);

        static::assertSame(12, $this->systemSettings->getInteger('test.key', 123));
    }

    /**
     * @covers ::get
     * @return void
     */
    public function testGetDefaultValue(): void
    {
        $defaultValue = 'some default value';
        $this->configFileService
            ->expects(static::once())
            ->method('get')
            ->with('test.key')
            ->willReturn('some default value');

        static::assertSame($defaultValue, $this->systemSettings->get('test.key', $defaultValue));
    }
}
