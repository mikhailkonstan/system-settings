<?php
declare(strict_types=1);


namespace Mkonstantinou\SystemSettings\Tests\Unit\Classes;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Mkonstantinou\SystemSettings\Classes\SystemSettings;
use Mkonstantinou\SystemSettings\Models\SystemSettingsItem;
use Mkonstantinou\SystemSettings\Services\SystemSettingsConfigFileService;
use Mkonstantinou\SystemSettings\Services\SystemSettingsItemService;
use Tests\TestCase;

class SystemSettingsDatabaseTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testAddOrUpdateValues(): void
    {
        $systemSettings = new SystemSettings(new SystemSettingsItemService(), new SystemSettingsConfigFileService());

        $data = [
            'abbr' => 'test',
            'name' => 'Test name',
            'value' => json_encode(['key' => 'value'])
        ];

        static::assertTrue($systemSettings->addOrUpdateValues($data));

        // Assert value in db is correct
        $valueInDb = SystemSettingsItem::where('abbr', 'test')->first()->value;
        static::assertEquals(json_decode($valueInDb, true), ['key' => 'value']);
    }

    public function testAddOrUpdateValuesOnExisting(): void
    {
        // Old data
        $data = [
            'abbr' => 'test',
            'name' => 'Test name',
            'value' => json_encode(['key' => 'value'])
        ];

        // New data
        $newData = [
            'abbr' => 'test',
            'name' => 'Test name',
            'value' => json_encode(['key' => 'value', 'new_key' => 'new value'])
        ];

        $systemSettings = new SystemSettings(new SystemSettingsItemService(), new SystemSettingsConfigFileService());
        $systemSettings->add($data);

        static::assertTrue($systemSettings->addOrUpdateValues($newData));

        // Assert value in db is correct
        $valueInDb = SystemSettingsItem::where('abbr', 'test')->first()->value;
        $this->assertEquals(['key' => 'value', 'new_key' => 'new value'], json_decode($valueInDb, true));
    }

    public function testAddOrUpdateValuesWhenNoChangesArePresent(): void
    {
        $data = [
            'abbr' => 'test',
            'name' => 'Test name',
            'value' => json_encode(['key' => 'value'])
        ];

        $systemSettings = new SystemSettings(new SystemSettingsItemService(), new SystemSettingsConfigFileService());
        $systemSettings->add($data);
        static::assertTrue($systemSettings->addOrUpdateValues($data));

        // Assert value in db is correct
        $valueInDb = SystemSettingsItem::where('abbr', 'test')->first()->value;
        static::assertEquals(['key' => 'value'], json_decode($valueInDb, true));
    }
}
